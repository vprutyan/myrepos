<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Vladimir_Laynavfilter_Model_Form
{
    public function toOptionArray()
    {
        return array(
            array('value'=>1, 'label'=>Mage::helper('laynavfilter')->__('List')),
            array('value'=>2, 'label'=>Mage::helper('laynavfilter')->__('Dropdown')),
        );
    }

}