<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Vladimir_Laynavfilter_Block_Layer_Filter_Price extends Mage_Catalog_Block_Layer_Filter_Price
{
    public function __construct()
    {   
        parent::__construct();

        $this->_filterModelName = 'catalog/layer_filter_price';
        if(Mage::getStoreConfig('catalog/layered_navigation/form_display') == 2){
            $this->setTemplate('catalog/layer/dropdown.phtml');
        }        
    }

}