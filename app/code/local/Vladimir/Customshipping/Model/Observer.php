<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Vladimir_Customshipping_Model_Observer
{
    public function salesQuoteCollectTotalsBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();
       
        $store    = Mage::app()->getStore($quote->getStoreId());
        $items = $quote->getAllItems();

        $ship_price = $store->getConfig("carriers/flatrate/price");  
        $custom_ship_price = 0;
        $order_total = 0;
        $flat_rate_flag = false;

            foreach($items as $item) {            

                    $csr=Mage::getModel('catalog/product')->load($item->getProductId())->getCustomShippingRate();
                    $order_total = $order_total + $item->getPrice();
                    if($csr != ''){                   
                      $custom_ship_price = $custom_ship_price + $csr;                         
                    }
                    if($csr == ''){
                        $flat_rate_flag = true;
                    }
            }

        if($order_total > 250){
            $store->setConfig("carriers/flatrate/price", $ship_price);
        }   
        elseif(!$flat_rate_flag){
            $store->setConfig("carriers/flatrate/price", $custom_ship_price);                         
        }
        else{
            $store->setConfig("carriers/flatrate/price", $ship_price); 
        }
    }
}
