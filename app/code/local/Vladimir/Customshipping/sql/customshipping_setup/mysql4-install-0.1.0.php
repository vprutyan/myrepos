<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//$installer = $this;
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'custom_shipping_rate', array(
  'type'              => 'varchar',
  'backend'           => '',
  'frontend'          => '',
  'label'             => 'Custom Shipping Rate',
  'input'             => 'text',
  'class'             => '',
  //'source'            => 'catalog/product_attribute_source_layout',
  'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
  'visible'           => true,
  'required'          => false,
  'user_defined'      => false,
  'default'           => '',
  'searchable'        => false,
  'filterable'        => false,
  'comparable'        => false,
  'visible_on_front'  => true,
  'unique'            => false,
  'group'             => 'General'
));
//$installer->removeAttribute('catalog_product', 'custom_shipping_rate');
$installer->endSetup();
