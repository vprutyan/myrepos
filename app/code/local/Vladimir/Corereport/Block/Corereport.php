<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Crys
 * Date: 5/6/15
 * Time: 4:46 PM
 * To change this template use File | Settings | File Templates.
 */

class Vladimir_Corereport_Block_Corereport extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getCorereport() {
        if (!$this->hasData('corereport')) {
            $this->setData('corereport', Mage::registry('corereport'));
        }
        return $this->getData('corereport');
    }

}