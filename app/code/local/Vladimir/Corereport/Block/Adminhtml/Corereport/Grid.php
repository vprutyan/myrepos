<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Crys
 * Date: 5/6/15
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */

class Vladimir_Corereport_Block_Adminhtml_Corereport_Grid extends Mage_Adminhtml_Block_Report_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('corereportGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setSubReportSize(false);
    }

    protected function _prepareCollection() {
        parent::_prepareCollection();
        $this->getCollection()->initReport('corereport/corereport');
        return $this;
    }

    protected function _prepareColumns() {
        $this->addColumn('ordered_qty', array(
            'header'    => Mage::helper('reports')->__('Quantity Ordered'),
            'align'     =>'right',
            'index'     =>'ordered_qty',
            'total'     =>'sum',
            'type'      =>'number'
        ));
        $this->addColumn('item_id', array(
            'header' => Mage::helper('corereport')->__('Item ID'),
            'align' => 'right',
            'index' => 'item_id',
            'type'  => 'number',
            'total' => 'sum',
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('corereport')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('corereport')->__('XML'));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return false;
    }

    public function getReport($from, $to) {
        if ($from == '') {
            $from = $this->getFilter('report_from');
        }
        if ($to == '') {
            $to = $this->getFilter('report_to');
        }
        $totalObj = Mage::getModel('reports/totals');
        $totals = $totalObj->countTotals($this, $from, $to);
        $this->setTotals($totals);
        $this->addGrandTotals($totals);
        return $this->getCollection()->getReport($from, $to);
    }
}