<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Crys
 * Date: 5/6/15
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */

class Vladimir_Corereport_Block_Adminhtml_Corereport extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_corereport';
        $this->_blockGroup = 'corereport';
        $this->_headerText = Mage::helper('corereport')->__('CoreReport Report');
        parent::__construct();
        $this->_removeButton('add');
    }

}