<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Crys
 * Date: 4/10/15
 * Time: 5:05 PM
 * To change this template use File | Settings | File Templates.
 */

$installer = $this;
$installer->startSetup();
$this->addAttribute('customer_address', 'continent', array(
    'type' => 'varchar',
    'input' => 'select',
    'label' => 'Continent',
    'global' => 1,
    'visible' => 1,
    'required' => 1,
    'source'        => 'eav/entity_attribute_source_table',
	//'source'=> 'address/entity_continent',
    'user_defined' => 1,
    'visible_on_front' => 1,
    'option' => array (
        'values' =>
        array (
            0 => 'Europe',
            1 => 'Asia',
            2 => 'Africa',
            3 => 'North America',
            4 => 'South America',
            5 => 'Australia',
        ),
    ),
));
if (version_compare(Mage::getVersion(), '1.6.0', '<='))
{
    $customer = Mage::getModel('customer/address');
    $attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
    $this->addAttributeToSet('customer_address', $attrSetId, 'General', 'continent');
}
if (version_compare(Mage::getVersion(), '1.4.2', '>='))
{
    Mage::getSingleton('eav/config')
        ->getAttribute('customer_address', 'continent')
        ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address'))
        ->save();
}
//$tablequote = $this->getTable('sales/quote_address');
//$installer->run("ALTER TABLE  $tablequote ADD  `continent` varchar(255) NOT NULL");
//$tablequote = $this->getTable('sales/order_address');
//$installer->run("ALTER TABLE  $tablequote ADD  `continent` varchar(255) NOT NULL");
$installer->endSetup();