<?php
class Vladimir_Address_Model_Entity_Continent extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = array();
            $this->_options[] = array(
                'value' => '',
                'label' => 'Choose Continent ...'
            );
            $this->_options[] = array(
                'value' => 1,
                'label' => 'Europe'
            );
            $this->_options[] = array(
                'value' => 2,
                'label' => 'Asia'
            );
            $this->_options[] = array(
                'value' => 3,
                'label' => 'Africa'
            );

        }

        return $this->_options;
    }
}