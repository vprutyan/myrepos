<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Vlad
 * Date: 22.04.15
 * Time: 22:49
 * To change this template use File | Settings | File Templates.
 */

class Vladimir_Customreview_Block_Messages extends Mage_Review_Block_Form{

    public function getMessagesBlock(){

        $messages = Mage::getSingleton('core/session')->getMessages()->getLastAddedMessage();
        if(isset($messages )){
            $type = $messages->getType();
            $message = $messages->getCode();
            $html = '<ul class="messages">';
                $html .= '<li class="' . $type . '-msg">';
                    $html .= '<ul>';
                        $html.= '<li>';
                        $html.= $message;
                        $html.= '</li>';
                    $html .= '</ul>';
                $html .= '</li>';
            $html .= '</ul>';
            Mage::getSingleton('core/session')->setMessages(null);
            return $html;
        }
    }
}