<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Crys
 * Date: 4/7/14
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */

class Vladimir_Bestseller_Block_Bestseller extends Mage_Core_Block_Template{

    protected $_today;

    protected $_startDay;

    public function __construct(){
        parent::__construct();

        $this->setProductCollection($this->getProductCollection());
    }

    public function setVisibility(){
        $visibility     = array(
            Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
        );
        return $visibility;
    }

    public function setNumberProducts(){

      return Mage::getStoreConfig('bestseller_options/messages/qty_products');

    }

    public function setNumberDays(){

        return Mage::getStoreConfig('bestseller_options/messages/qty_days');
    }

    public function getProductCollection(){

        $this->_today = date('Y-m-d H:i:s');
        $this->_startDay = date('Y-m-d H:i:s', strtotime($this->_today . ' - ' . $this->setNumberDays() . ' day'));

        $_productCollection = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect('*')
            ->addOrderedQty($this->_startDay,$this->_today)
            ->addAttributeToFilter('visibility', $this->setVisibility())
            ->setStoreId($this->getStoreId())
            ->addStoreFilter($this->getStoreId())
            ->setOrder('ordered_qty', 'desc')
            ->setPageSize($this->setNumberProducts());
        return $_productCollection;
    }

    public function getStoreId(){

        return Mage::app()->getStore()->getId();
    }

}