<?php

class Vladimir_Bestseller_Model_Observer extends Varien_Event_Observer
{
    public function addHandle(Varien_Event_Observer $observer) {
        $columnSide = Mage::getStoreConfig('weblog_options/messages/select_side');

        if($this->getUrl('') == $this->getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true)))
        {

            if ($columnSide == 2) {

                $update = $observer->getEvent()->getLayout()->getUpdate();
                $update->addHandle('side_right');

            }
            else if ($columnSide == 1) {

                $update = $observer->getEvent()->getLayout()->getUpdate();
                $update->addHandle('side_left');

            }
        }

    }
}