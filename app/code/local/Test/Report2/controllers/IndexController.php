<?php
class Test_Report2_IndexController extends Mage_Adminhtml_Controller_Report_Abstract
{
    
    
    public function bestsellersskuAction()
    {
    	$this->loadLayout();
    	$this->_title($this->__('Reports'))->_title($this->__('Products'))->_title($this->__('Bestsellers'));

    	$this->_initAction()
    	->_setActiveMenu('report/products/bestsellers')
    	->_addBreadcrumb(Mage::helper('adminhtml')->__('Products Bestsellers Report'), Mage::helper('adminhtml')->__('Products Bestsellers Report'));

    	$filterFormBlock = $this->getLayout()->getBlock('grid.filter.form');
    	$gridBlock = $this->getLayout()->getBlock('report_sales_bestsellers.grid');

    	$this->_initReportAction(array(
    			$gridBlock,
    			$filterFormBlock
    	));

    	$this->renderLayout();        
    }

    
}
