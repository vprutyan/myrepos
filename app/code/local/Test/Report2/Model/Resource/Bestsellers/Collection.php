<?php

class Test_Report2_Model_Resource_Bestsellers_Collection extends Mage_Sales_Model_Resource_Report_Collection_Abstract

{
	/**
	 * Rating limit
	 *
	 * @var int
	 */
	protected $_ratingLimit        = 5;

	/**
	 * Columns for select
	 *
	 * @var array
	 */
	protected $_selectedColumns    = array();

	/**
	 * Mysql Date format for 'period' columns
	 *
	 * @var string
	 */
	protected $_periodFormat = '%Y-%m-%d';

	/**
	 * Initialize custom resource model
	 *
	 */
	public function __construct()
	{
		parent::_construct();
		$this->setModel('adminhtml/report_item');
		$this->_resource = Mage::getResourceModel('sales/report')->init('sales/bestsellers_aggregated_daily');
		$this->setConnection($this->getResource()->getReadConnection());
		// overwrite default behaviour
		$this->_applyFilters = false;
	}

	/**
	 * Retrieve columns for select
	 *
	 * @return array
	 */
	protected function _getSelectedColumns()
	{
		$adapter = $this->getConnection();
		if (!$this->_selectedColumns) {
			if ($this->isTotals()) {
				$agrCol = $this->getAggregatedColumns();
				$this->_selectedColumns = $this->getAggregatedColumns();
			}
			$this->_selectedColumns = array(
					'period'         =>  sprintf('%s', $adapter->getDateFormatSql('period', $this->_periodFormat)),
					'qty_ordered'    => 'SUM(qty_ordered)',
					'product_id'     => 'product_id',
					'product_name'   => 'MAX(product_name)',
					'product_price'  => 'MAX(product_price)',
					'skuu'			 => 'MAX(t2.sku)',
					'orders_list'      => 't3.order_id',
			);
		}
		return $this->_selectedColumns;
	}

	protected function _makeBoundarySelect($from, $to)
	{
		$adapter = $this->getConnection();
		$sel     = $adapter->select() ;
		$sel     = $this->_formQuery ($sel);

		$sel ->where("period >= ?", $from)
		->where("period <= ?", $to)
		->group("DATE_FORMAT(period, '". $this->_periodFormat."')" )
		->group( 'product_id')
		->order('qty_ordered DESC')
		->limit($this->_ratingLimit);

		$this->_applyStoresFilterToSelect($sel);
		return $sel;
	}

	protected function _formQuery ($select)
	{
		$mainTable = $this->getTable('sales/bestsellers_aggregated_daily');

		$select = $select->joinLeft(
				array('t2'=>'catalog_product_entity'),
				$mainTable.'.product_id = t2.entity_id ',
				array ());

		$tmpSubQ = "(select DATE_FORMAT(it.created_at,'". $this->_periodFormat."') as created,
				it.product_id as productid,
				GROUP_CONCAT(it.order_id) as order_id
				from sales_flat_order_item  as it
				where it.product_type = 'simple'
				GROUP BY created,  productid )";

		$select = $select->joinLeft(
				array('t3'=>new Zend_Db_Expr($tmpSubQ) ),
				$mainTable.".product_id = t3.productid  AND  DATE_FORMAT(".$mainTable.".period,'". $this->_periodFormat ."' )  = t3.created",
				array ());

		$select->from($mainTable, $this->_getSelectedColumns());
		return $select;
	}

	protected function _initSelect()
	{
		if ('year' == $this->_period) {
			$this->_periodFormat = '%Y' ;
		} elseif ('month' == $this->_period) {
			$this->_periodFormat = '%Y-%m' ;
		} else {
			$this->_periodFormat = '%Y-%m-%d' ;
		}
		
		$select = $this->getSelect();
		$select = $this->_formQuery($select);
		$select->group(array('product_id', "DATE_FORMAT(period, '". $this->_periodFormat."')"  ) );
		$select->where('rating_pos <= ?', $this->_ratingLimit);

		return $this;
	}

	public function getSelectCountSql()
	{
		$this->_renderFilters();
		$select = clone $this->getSelect();
		$select->reset(Zend_Db_Select::ORDER);
		return $this->getConnection()->select()->from($select, 'COUNT(*)');
	}

	public function addStoreRestrictions($storeIds)
	{
		if (!is_array($storeIds)) {
			$storeIds = array($storeIds);
		}
		$currentStoreIds = $this->_storesIds;
		if (isset($currentStoreIds) && $currentStoreIds != Mage_Core_Model_App::ADMIN_STORE_ID
				&& $currentStoreIds != array(Mage_Core_Model_App::ADMIN_STORE_ID)) {
			if (!is_array($currentStoreIds)) {
				$currentStoreIds = array($currentStoreIds);
			}
			$this->_storesIds = array_intersect($currentStoreIds, $storeIds);
		} else {
			$this->_storesIds = $storeIds;
		}

		return $this;

	}

	protected function _beforeLoad()
	{
		parent::_beforeLoad();
		$this->_applyStoresFilter();

		if ($this->_period) {
			//
			$selectUnions = array();

			// apply date boundaries (before calling $this->_applyDateRangeFilter())
			$dtFormat   = Varien_Date::DATE_INTERNAL_FORMAT;
			$periodFrom = (!is_null($this->_from) ? new Zend_Date($this->_from, $dtFormat) : null);
			$periodTo   = (!is_null($this->_to)   ? new Zend_Date($this->_to,   $dtFormat) : null);

			if ('year' == $this->_period) {

				if ($periodFrom) {
					// not the first day of the year
					if ($periodFrom->toValue(Zend_Date::MONTH) != 1 || $periodFrom->toValue(Zend_Date::DAY) != 1) {
						$dtFrom = $periodFrom->getDate();
						// last day of the year
						$dtTo = $periodFrom->getDate()->setMonth(12)->setDay(31);
						if (!$periodTo || $dtTo->isEarlier($periodTo)) {
							$selectUnions[] = $this->_makeBoundarySelect(
									$dtFrom->toString($dtFormat),
									$dtTo->toString($dtFormat)
							);

							// first day of the next year
							$this->_from = $periodFrom->getDate()
							->addYear(1)
							->setMonth(1)
							->setDay(1)
							->toString($dtFormat);
						}
					}
				}

				if ($periodTo) {
					// not the last day of the year
					if ($periodTo->toValue(Zend_Date::MONTH) != 12 || $periodTo->toValue(Zend_Date::DAY) != 31) {
						$dtFrom = $periodTo->getDate()->setMonth(1)->setDay(1);  // first day of the year
						$dtTo = $periodTo->getDate();
						if (!$periodFrom || $dtFrom->isLater($periodFrom)) {
							$selectUnions[] = $this->_makeBoundarySelect(
									$dtFrom->toString($dtFormat),
									$dtTo->toString($dtFormat)
							);

							// last day of the previous year
							$this->_to = $periodTo->getDate()
							->subYear(1)
							->setMonth(12)
							->setDay(31)
							->toString($dtFormat);
						}
					}
				}

				if ($periodFrom && $periodTo) {
					// the same year
					if ($periodFrom->toValue(Zend_Date::YEAR) == $periodTo->toValue(Zend_Date::YEAR)) {
						$dtFrom = $periodFrom->getDate();
						$dtTo = $periodTo->getDate();
						$selectUnions[] = $this->_makeBoundarySelect(
								$dtFrom->toString($dtFormat),
								$dtTo->toString($dtFormat)
						);

						$this->getSelect()->where('1<>1');
					}
				}

			}
			else if ('month' == $this->_period) {
				if ($periodFrom) {
					// not the first day of the month
					if ($periodFrom->toValue(Zend_Date::DAY) != 1) {
						$dtFrom = $periodFrom->getDate();
						// last day of the month
						if (!$periodTo) {
							$dtTo = $periodFrom->getDate()->addMonth(1)->setDay(1)->subDay(1);
						} else {
							$dtTo = $periodTo->getDate();
						}
						if (!$periodTo || $dtTo->isEarlier($periodTo)) {
							$selectUnions[] = $this->_makeBoundarySelect(
                                $dtFrom->toString($dtFormat),
                                $dtTo->toString($dtFormat)
                            );

                            // first day of the next month
                            $this->_from = $periodFrom->getDate()->addMonth(1)->setDay(1)->toString($dtFormat);
                        }
                    }
                }

                if ($periodTo) {
                    // not the last day of the month
                    if ($periodTo->toValue(Zend_Date::DAY) != $periodTo->toValue(Zend_Date::MONTH_DAYS)) {
                    	if (!$periodFrom) {
                        	$dtFrom = $periodTo->getDate()->setDay(1);  // first day of the month
                    	} else {
                    		$dtFrom = $periodFrom->getDate();
                    	}
                        $dtTo = $periodTo->getDate();
                        if (!$periodFrom || $dtFrom->isLater($periodFrom)) {
                            $selectUnions[] = $this->_makeBoundarySelect(
                                $dtFrom->toString($dtFormat),
                                $dtTo->toString($dtFormat)
                            );

                            // last day of the previous month
                            $this->_to = $periodTo->getDate()->setDay(1)->subDay(1)->toString($dtFormat);
                        }
                    }
                }

                if ($periodFrom && $periodTo) {
                    // the same month
                    if ($periodFrom->toValue(Zend_Date::YEAR) == $periodTo->toValue(Zend_Date::YEAR)
                        && $periodFrom->toValue(Zend_Date::MONTH) == $periodTo->toValue(Zend_Date::MONTH)
                    ) {
                        $dtFrom = $periodFrom->getDate();
                        $dtTo = $periodTo->getDate();
                        $selectUnions[] = $this->_makeBoundarySelect(
                            $dtFrom->toString($dtFormat),
                            $dtTo->toString($dtFormat)
                        );

                        $this->getSelect()->where('1<>1');
                    }
                }
			}
            $this->_applyDateRangeFilter();

            // add unions to select
            if ($selectUnions) {
                $unionParts = array();
                $cloneSelect = clone $this->getSelect();
                $helper = Mage::getResourceHelper('core');
                $unionParts[] = '(' . $cloneSelect . ')';
                foreach ($selectUnions as $union) {
                    $query = $helper->getQueryUsingAnalyticFunction($union);
                    $unionParts[] = '(' . $query . ')';
                }
                $this->getSelect()->reset()->union($unionParts, Zend_Db_Select::SQL_UNION_ALL);
            }

            if ($this->isTotals()) {
                // calculate total
                $cloneSelect = clone $this->getSelect();
                $this->getSelect()->reset()->from($cloneSelect, $this->getAggregatedColumns());
            } else {
                // add sorting
                $this->getSelect()->order(array('period ASC', 'qty_ordered DESC'));
            }
        }

        return $this;

	}
}