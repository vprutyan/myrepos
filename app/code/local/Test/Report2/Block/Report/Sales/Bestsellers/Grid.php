<?php
class Test_Report2_Block_Report_Sales_Bestsellers_Grid extends 
Mage_Adminhtml_Block_Report_Grid_Abstract
{
    protected $_columnGroupBy = 'period';
    protected $_storeIds      = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->setCountTotals(true);
    }

    public function getResourceCollectionName()
    {
    	return 'report2/bestsellers_collection';
    }

    protected function _prepareColumns()
    {
    	
        $this->addColumn('period', array(
            'header'        => Mage::helper('sales')->__('Period'),
            'index'         => 'period',
            'width'         => 100,
            'sortable'      => false,
            'period_type'   => $this->getPeriodType(),
            'renderer'      => 'adminhtml/report_sales_grid_column_renderer_date',
            'totals_label'  => Mage::helper('adminhtml')->__('Total'),
            'html_decorators' => array('nobr'),
        ));

        $this->addColumn('product_name', array(
            'header'    => Mage::helper('sales')->__('Product Name'),
            'index'     => 'product_name',
            'type'      => 'string',
            'sortable'  => false
        ));
			if ($this->getFilterData()->getStoreIds()) {
				$this->setStoreIds(explode(',', $this->getFilterData()->getStoreIds()));
			}
        $currencyCode = $this->getCurrentCurrencyCode();

        $this->addColumn('product_price', array(
            'header'        => Mage::helper('sales')->__('Price'),
            'type'          => 'currency',
            'currency_code' => $currencyCode,
            'index'         => 'product_price',
            'sortable'      => false,
            'rate'          => $this->getRate($currencyCode),
        ));

        $this->addColumn('qty_ordered', array(
            'header'    => Mage::helper('sales')->__('Quantity Ordered'),
            'index'     => 'qty_ordered',
            'type'      => 'number',
            'total'     => 'sum',
            'sortable'  => false
        ));

        $this->addColumn('skuu', array(
        		'header'    => Mage::helper('report2')->__('Product Sku'),
        		'index'     => 'skuu',
        		'type'      => 'string',
        		'sortable'  => false
        ));
        $this->addColumn('orders_list', array(
        		'header'    => Mage::helper('report2')->__('Orders list'),
        		'index'     => 'orders_list',
        		'type'      => 'string',
        		'sortable'  => false
        ));        

        return parent::_prepareColumns();
    }
}
