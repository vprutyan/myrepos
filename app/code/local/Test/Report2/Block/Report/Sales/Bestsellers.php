<?php

class Test_Report2_Block_Report_Sales_Bestsellers extends Mage_Adminhtml_Block_Widget_Grid_Container
{

	protected $_blockGroup = 'report2';
	
	public function __construct()
    {
        $this->_controller = 'report_sales_bestsellers';
        $this->_headerText = Mage::helper('sales')->__('Products Bestsellers Report with SKU');
        parent::__construct();
        $this->setTemplate('report/grid/container.phtml');
        $this->_removeButton('add');
        $this->addButton('filter_form_submit', array(
            'label'     => Mage::helper('reports')->__('Show Report'),
            'onclick'   => 'filterFormSubmit()'
        ));
    }

    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/bestsellerssku', array('_current' => true));
    }
}
