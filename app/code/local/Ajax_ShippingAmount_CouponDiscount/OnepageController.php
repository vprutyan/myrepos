<?php
require_once 'Mage/Checkout/controllers/OnepageController.php';
class Speroteck_Newcheckout_OnepageController extends Mage_Checkout_OnepageController
{
    //start setUseCredit Session if customer chose to use credits for the current order
	public function saveCreditAction()
	{
		if ($this->_expireAjax()) {
			return;
		}
		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getPost('credit', array());

			$result = $this->getOnepage()->saveCredit($data);

			if (!isset($result['error'])) {
				Mage::getSingleton('core/session')->setUseCredit($data['use']);
				$result['goto_section'] = 'billing';
			}

			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
		}
	}
    public function getGrandTotalAction(){

        $qoute = Mage::getModel('checkout/session')->getQuote();



        $couponCode = $qoute->getCouponCode();
        $coupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
        $rule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());
        $discountAmount = $rule->getDiscountAmount();


        $shippingMethod = $qoute->getShippingAddress();
        $shippingAmount = $shippingMethod['shipping_amount'];
        $result['payment_grand_total'] = $shippingMethod['grand_total'];
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

}
