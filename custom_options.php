<?php

require_once 'app/Mage.php';
Mage::app('default');
 
$productIds = array(166,162); // Product Ids you want to add options

$option = array(
        'title' => 'Choose Scratch and Sniff Sticker Placement: ',
        'type' => 'radio',
        'is_require' => 1,
        'price' => 0.00,
        'price_type' => 'fixed',
        'sku' => 'testsku',
        'sort_order' => 1,
    "values" => array(
    "0" => array(
        
        "title" => "Center of button",
        "price" => 0.00,
        "price_type" => "fixed",
        "sku" => "center",
        "sort_order" => 1
      ),  
    "1" => array(
        
        "title" => "Other. Please specify:",
        "price" => 0.00,
        "price_type" => "fixed",
        "sku" => "other",
        "sort_order" => 2
      )
    )
);

foreach ($productIds as $productId) {
        $product = Mage::getModel('catalog/product')->load($productId);
        $optionInstance = $product->getOptionInstance()->unsetOptions();
 
        $product->setHasOptions(1);
        if (isset($option['is_require']) && ($option['is_require'] == 1)) {
                $product->setRequiredOptions(1);
        }
        $optionInstance->addOption($option);
        $optionInstance->setProduct($product);
        $product->save();
 
}